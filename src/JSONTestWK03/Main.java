package JSONTestWK03;
// Import Jackson Libraries
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import step2.Bible;
import step2.CreateJSON;

//Import Java io, net, and utils
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpClient;
import java.util.List;

public class Main {

    // Define the URL to parse
    private static final String POSTS_API_URL = "https://jsonplaceholder.typicode.com/albums";


    public static void main( String[] args ) throws IOException, InterruptedException {
        // Build a new HTTP Session.
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .header("accept", "application/json")
                .uri(URI.create(POSTS_API_URL))
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        //Bring JSON into the Java Objects
        ObjectMapper mapper = new ObjectMapper();
        List<Post> posts = mapper.readValue(response.body(), new TypeReference<List<Post>>() {
        });

        // Print out the post data
        System.out.print("Hello, This takes data from a website API, converts it to JSON and then adds to object ");
        posts.forEach(System.out::println);
        System.out.print(" \n");
        System.out.print("This takes text, converts it to object, and then posts it as JSON-http ");
        System.out.print(" \n");

        // Assignment Part 2 - Post to JSON
        Bible bib = new Bible();
        bib.setBook("John");
        bib.setChapter(3);
        bib.setVerse(16);
        bib.setVersetext("For God so loved the world, that he gave his only begotten Son, that whosoever believeth in him should not perish, but have everlasting life");

        String json = CreateJSON.bibleToJSON(bib);
        System.out.println(json);


    }
    // Push Bible data to JSON
    public static String bibleToJSON(Bible bible) {

        ObjectMapper mapper = new ObjectMapper();
        String a = "";

        try {
            a = mapper.writeValueAsString(bible);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return a;
    }


}