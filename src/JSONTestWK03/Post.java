package JSONTestWK03;

public class Post {

    private int userId;
    private int id;
    private String title;

    public int getuserId() {
        return userId;
    }

    public void setuserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return                "id=" + id +
                ", title='" + title + '\'' ;
    }
}

