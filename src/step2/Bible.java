// Getters and Setter for Bible to JSON

package step2;

public class Bible {
    private String book;
    private int chapter;
    private int verse;
    private String versetext;

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public int getChapter() {
        return chapter;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }

    public int getVerse() {
        return verse;
    }

    public void setVerse(int verse) {
        this.verse = verse;
    }

    public String getVersetext() {
        return versetext;
    }

    public void setVersetext(String versetext) {
        this.versetext = versetext;
    }


}
