package step2;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class CreateJSON {

    public static String bibleToJSON(Bible bible) {

        ObjectMapper mapper = new ObjectMapper();
        String a = "";

        try {
            a = mapper.writeValueAsString(bible);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return a;
    }



    public static void test(String[] args) {

        Bible bib = new Bible();
        bib.setBook("John");
        bib.setChapter(3);
        bib.setVerse(16);
        bib.setVersetext("For God so loved the world, that he gave his only begotten Son, that whosoever believeth in him should not perish, but have everlasting life");

        String json = CreateJSON.bibleToJSON(bib);
        System.out.println(json);


    }

}
